import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import appReducer from 'App/reducer';
import Header from 'App/components/Header';

import style from './app.css';

const store = createStore(appReducer);

function App() {
  return (
    <Provider store={store}>
      <div className={style.appRoot}>
        <Header />
        Hello World!!
      </div>
    </Provider>
  );
}

ReactDOM.render(<App />, document.getElementById('app'));
