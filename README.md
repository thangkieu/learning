## Webpack playaroud
@author: Thang Kieu

### Tech inside
1. [Webpack](https://webpack.github.io/) - _Tool to build app, compile css module, babel syntax, react component_
2. [ReactJS](https://facebook.github.io/react/) - _JS framework_
3. ES6 - _New syntax for javascript_
4. [ESLint](https://github.com/airbnb/javascript) - _JS convention, follow eslint of airbnb_
5. [CSS Modules](https://github.com/css-modules/css-modules), [CSSNext](http://cssnext.io/) - _New syntax of css syntax_
6. PostCSS ( _Autoprefixer_, _Nesting_, _Calculator_ **put anythings you want :)** )



* _Prevent Build process if there is any error_
