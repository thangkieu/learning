const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

// CSS Next Feature
const autoprefixer = require('autoprefixer');
const cssCalc = require('postcss-calc');
// const cssNesting = require('postcss-nesting');


const sourcePath = {
  app: path.join(__dirname, '/app'),
  dist: path.join(__dirname, '/dist/'),
  assets: path.join(__dirname, '/assets/')
};

module.exports = {
  entry: path.join(sourcePath.app, '/app.js'),
  output: {
    filename: 'app.js',
    path: sourcePath.dist
  },
  devtool: 'source-map',
  module: {
    rules: [{
      enforce: 'pre',
      test: /\.js$/,
      include: [sourcePath.app],
      use: [
        'eslint-loader'
      ]
    }, {
      test: /\.js$/,
      include: [sourcePath.app],
      use: [
        'babel-loader'
      ]
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {
            camelCase: true,
            localIdentName: '[name]_[local]_[hash:base64:3]',
            importLoaders: 1,
            sourceMap: 1
          }
        }, {
          loader: 'postcss-loader',
          options: {
            sourceMap: 'inline',
            plugins: () => ([
              autoprefixer,
              cssCalc,
              // cssNesting
            ])
          }
        }]
      })
    }]
  },
  plugins: [
    new ExtractTextPlugin('css/styles.css'),
    new webpack.NoErrorsPlugin()
  ],
  resolve: {
    alias: {
      App: sourcePath.app,
      Assets: sourcePath.assets
    },
    modules: ['node_modules']
  }
};
