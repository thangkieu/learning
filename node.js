const http = require('http');
const port = 3000;
const fs = require('fs');

const requestHandler = (req, res) => {
  if (req.url === '/') {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    fs.createReadStream('index.html').pipe(res);
    // fs.readFile('index.html', (error, data) => {
    //   res.writeHead(200, { 'Content-Type': 'text/html' });
    //   res.write(data);
    //   res.end();
    // });
  }
};

// create an instance of http server
const server = http.createServer(requestHandler);
server.listen(port, error => {
  if (error) {
    return console.log('Server Error ', error);
  }

  console.log('Server is running on port ' + port);
})